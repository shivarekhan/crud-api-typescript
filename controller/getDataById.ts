import pool from "../model/connection";
import { Request, Response } from "express";

const getProductById = (req: Request, res: Response) => {
  const id: number = parseInt(req.params.id);
  pool.query(
    `SELECT * FROM fakeproducts WHERE productId = ${id}`,
    (error, data) => {
      if (!error) {
        res.status(200).send(data.rows);
      }
    }
  );
};

export default getProductById;
