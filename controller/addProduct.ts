import pool from "../model/connection";
import { Request, Response } from "express";

const addProduct = (req: Request, res: Response) => {
  const { Productid, title, price, category } = req.body;
  if (Productid != null && title != null && price != null && category != null) {
    pool.query(
      "SELECT entry FROM fakeproducts entry WHERE productid=$1",
      [Productid],
      (error, data) => {
        if (data.rows.length) {
          res.send("Product id already taken");
        } else {
          pool.query(
            `INSERT INTO fakeproducts(productid,title,price,category) VALUES ($1,$2,$3,$4)`,
            [Productid, title, price, category],
            (error, data) => {
              if (!error) {
                res.send("product added");
              }
            }
          );
        }
      }
    );
  } else {
    res.send("invalid data");
  }
};

export default addProduct;
