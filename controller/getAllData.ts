import pool from "../model/connection";
import { Request, Response } from "express";

const getAllData = (req: Request, res: Response) => {
  pool.query(`SELECT * FROM fakeproducts`, (error, data) => {
    if (!error) {
      res.status(200).send(data.rows);
    }
  });
};

export default getAllData;
