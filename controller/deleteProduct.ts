import pool from "../model/connection";
import { Request, Response } from "express";

const deleteProduct = (req: Request, res: Response) => {
  const id: number = parseInt(req.params.id);
  pool.query(
    `DELETE FROM fakeproducts WHERE productId = ${id}`,
    (error, data) => {
      if (!error) {
        res.send("product removed");
      }
    }
  );
};

export default deleteProduct;
