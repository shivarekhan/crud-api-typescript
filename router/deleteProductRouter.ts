import { Router, Request, Response } from "express";
import deleteProduct from "../controller/deleteProduct";

const deleteProductRouter: Router = Router();

deleteProductRouter.delete("/:id", deleteProduct);

export default deleteProductRouter;
