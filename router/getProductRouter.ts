import { Router, Request, Response } from "express";
import getAllData from "../controller/getAllData";
import getDataById from "../controller/getDataById";

const getProductRouter: Router = Router();

getProductRouter.get("/", getAllData);
getProductRouter.get("/:id", getDataById);

export default getProductRouter;
