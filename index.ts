import express, { Express, Request, Response } from "express";
import getProductRouter from "./router/getProductRouter";
import addProductRouter from "./controller/addProduct";
import deleteProductRouter from "./router/deleteProductRouter";

const app: Express = express();
const port: number | string = process.env.PORT || 5000;
app.use(express.json());

app.get("/", (req: Request, res: Response) => {
  res.send("REST_API using typescript and nodejs");
});

app.use("/getProducts", getProductRouter);
app.use("/addProduct", addProductRouter);
app.use("/deleteProduct", deleteProductRouter);

app.listen(port, () => {
  console.log(`server is running on port ${port}`);
});
